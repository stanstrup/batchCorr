% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/batchAlign5.R
\name{grabAlign}
\alias{grabAlign}
\title{BA: Grab batch relevant data for a subtype of samples}
\usage{
grabAlign(XS, batch, grp)
}
\arguments{
\item{XS}{an XSMS object}

\item{batch}{a batch identifier}

\item{grp}{a sample type identifier}
}
\value{
a peak table with samples as rows and features as columns, features (colnames) named by 'm/z@rt'
}
\description{
grabAlign will bring out sampletype:'grp' from batch:'batch' of an XCMS object
}
